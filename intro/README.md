# Introduction

Learning makes us human. Some would even say it is the reason for our existence.

The best way to learn has always been rather obvious. One person shares or shows what they know. The other receives and practices it while the person with the knowledge assists, adjusts, provides ways to remember, and coaches the person learning until they have mastered the knowledge and can demonstrate their mastery to themselves as well as others.

Learning, therefore, requires repetition, communication, empathy, creativity, and unwavering trust between the learner and mentor. Naturally, then, the best learning happens one-on-one between friends or peers, two people who mutually respect one another with one having simply learned the knowledge before the other. This type of learning dates back to before recorded history and has been called many things: master and apprentice, guru and disciple, Jedi and Padawan. 

* Why then has one-on-one learning ceased in our public schools and organizations? 

* Why don't teachers and students trust each other?

* Why do students sit in rows of 40 or more in a single class room? 

* Why aren't students allowed to see the results of their own tests? 

* Why are assessments of physical skills done with multiple choice exams?

* Why do college degrees cost more than \$100K on average today?

* Why do colleges even exist?

* Why has the overall quality of education continued to spiral downward?

These questions continue to be a source of much discussion, debate, and frankly negativity. This book does not attempt to address them. 

Instead, this guidebook builds on the observable truth that one-on-one mentoring, promoting open, shared but independent learning in individuals, without unsustainable dependencies, is simply and undisputedly the best way to promote human learning and progress and always has been. The important question is, "How can we maximize learning this way whenever and wherever possible?"

